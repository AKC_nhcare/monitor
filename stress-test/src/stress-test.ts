
import axios from "axios";
import * as moment from "moment";
import {Promise} from "es6-promise"
import {DBService} from "./DBService"





    export class FormTester{

        answers:any[]
        encounterId:string
        patientId:string
        interval:number
        frequency:number
        form_url:string 
        timerReference:any
        counter:number
        isAsync:boolean
        isDebug:boolean
        host:string

        constructor(
                answers:any[] =[1,100100000,1,1,0,1,1,0,0,1,1,1,7,3,0]
                ,encounterId:string = "7843903"
                ,patientId:string ="333561"
                ,form_url:string="inject_dast"
                ,interval:number= 5
                ,frequency:number= 2
                ,isAsync:boolean=false
                ,isDebug:boolean=false


        ){
            this.answers=answers;
            this.encounterId=encounterId;
            this.patientId=patientId;
            this.form_url=form_url;
            this.interval= process.argv[2]? parseInt(process.argv[2]):interval;
            this.frequency=process.argv[3]? parseInt(process.argv[3]):frequency;
            this.isAsync=true;
            this.host= this.isDebug?'localhost':'10.18.6.41';
            this.counter=0;
        }


        submitForm( increment:number){
                let payload = {
                    'answers': this.answers.toString(),
                    'encounterId': this.encounterId,
                    'patientId' : this.patientId
                }
            
                axios
                    .post(`http://${this.host}/Api/alerts/${this.form_url}`, payload)
                    .then(result => {
                        console.log(`ecw form submitted ${increment}`) 
                       this.onEcwSubmitComplete(payload.answers,increment);
                    })     
                    .catch(error => {
                        let msg = error.response.data.Message ? error.response.data.Message.toString(): error.message;
                        console.log(`error writting form ${increment} to ecw `)  
                       this.onEcwSubmitFailure(payload.answers, msg,increment);
                                    
                    })
        } 
            
         
        onEcwSubmitComplete(answers:any,increment:number){
            let strSql=
            `insert into stress (timestamp,url, patientId, encounterId, data) 
              values ( '${moment().format('YYYY-MM-DD hh:mm')}','http://${this.host}/api/alerts/inject_dast',${this.patientId},${this.encounterId},'${answers.toString()}')`
           
            DBService.mysqlQuery(strSql,
            (results:any)=>{
                if(results.message =="")
                    console.log("inserted stress test audit in DB")
                    else
                    console.log("error inserting stress test audit in DB" + results.message)
                   this.onDBSubmitComplete(increment);
            })
        }
        
        onEcwSubmitFailure(answers:any, err:string,increment:number){
            let strSql=
            `insert into stress (timestamp, url, errorDesc, patientId, encounterId, data ) 
              values ( '${moment().format('YYYY-MM-DD hh:mm')}', 'http://${this.host}/api/alerts/inject_dast',
                            '${err}', ${this.patientId}, ${this.encounterId}, '${answers.toString()}')`
           
            DBService.mysqlQuery(strSql,
            (results:any)=>{
                if(results.message =="")
                console.log("inserted stress test audit in DB")
                else
                console.log("error inserting stress test audit in DB" + results.message)
               this.onDBSubmitComplete(increment);
            })
        }

        onDBSubmitComplete(incrementId:number){
          
            if(this.isAsync == false)
            {
                this.counter++;           
                if( this.counter < this.frequency){
                    let submitId=this.counter;
                    this.submitForm(submitId);
                }
                    
                else
                    {
                     
                        console.log(`stress test serial completed after ${this.frequency} requests `)
                        process.exit();
                        
                    }
                   
            }
            else{
                if(incrementId >= this.frequency){
                    console.log(`stress test async completed after ${this.frequency} requests `)
                    clearInterval(this.timerReference);
                    process.exit();
                }



            }
          
        }


        public runStressTestAsync(){
            console.log(`begining stress test async interval every ${this.interval} seconds, repeated ${this.frequency} times`)
                     
            this.timerReference=setInterval( ()=>{
                this.counter++;
                if(this.counter <= this.frequency){
                    let submitId=this.counter;
                    this.submitForm(submitId);  
                }
                else{
                   // clearInterval(this.timerReference);
                   //process.exit();
                }                      
            }, this.interval * 1000)

        }


        // you may be wondering why with promises and arrow functions 
        // those techniques are not being used for this test which relies on callbacks
        // the reason is a bug in node.js - this particular version + typescript which give a heap out of memory 
        // error with repeated nested callbacks - probably can refactor in another version
        public runStressTestSerial(){
            console.log("begining stress test serial run")
          this.counter ++;
            let submitId=this.counter;
            this.submitForm(submitId);
        }



    }

    let Tester1 = new FormTester();
    //Tester1.runStressTestSerial();
    Tester1.runStressTestAsync();


