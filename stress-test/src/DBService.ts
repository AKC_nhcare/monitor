import * as mysql from "mysql"


export class DBService{


    private static user = "root";
    private static password = "reg1234";
    private static host = "192.168.116.24";
    private static db = "monitor";
    public static dbconn=DBService.getConnection();

    

    private static getConnection(){
       
       var conn= mysql.createConnection(
            {
                host:DBService.host,
                user:DBService.user,
                password:DBService.password,
                database:DBService.db
            }
        );
        conn.connect();
        return conn;
    }

    public static mysqlQuery( strSql:string, callback:Function){
        DBService.dbconn.query(strSql, (error, results)=>{
                    if(error == null){                    
                        callback(results);
                    }                       
                    else {                       
                        callback(error);
                    }                       
                })
           

    }
}



