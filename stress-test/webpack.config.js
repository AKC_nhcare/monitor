
//https://github.com/mzabriskie/axios/issues/870
//https://github.com/mzabriskie/axios/issues/456#issuecomment-285287911
/* if wepack target is not node, axios is expecting httprequest to be available in the 
browser so you cant subsequently run node bundle */

const webpack = require('webpack');
const destinationPath = __dirname + '/dist';

module.exports={
target:'node',


resolve:{
    extensions:['.ts','.tsx','.js','.json']
},
entry:'./src/stress-test.ts',

devtool: 'source-map',

module:{
    rules:[
        {
            test:/\.tsx?$/,
             exclude: /node_modules/,
            use:[{loader:'awesome-typescript-loader'}]
        },
        {
             test:/\.js$/,
              exclude: /node_modules/,
             enforce:"pre", 
             use:[{loader:"source-map-loader"}]
        }
       
    ]

   

},
output: {
    filename: 'bundledjs.js',
    path: destinationPath
  },



}