"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mysql = require("mysql");
var DBService = (function () {
    function DBService() {
    }
    DBService.getConnection = function () {
        var conn = mysql.createConnection({
            host: DBService.host,
            user: DBService.user,
            password: DBService.password,
            database: DBService.db
        });
        conn.connect();
        return conn;
    };
    DBService.mysqlQuery = function (strSql, callback) {
        DBService.dbconn.query(strSql, function (error, results) {
            if (error == null) {
                callback(results);
            }
            else {
                callback(error);
            }
        });
    };
    DBService.user = "root";
    DBService.password = "reg1234";
    DBService.host = "192.168.116.24";
    DBService.db = "monitor";
    DBService.dbconn = DBService.getConnection();
    return DBService;
}());
exports.DBService = DBService;
//# sourceMappingURL=DBService.js.map