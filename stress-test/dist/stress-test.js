"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var moment = require("moment");
var DBService_1 = require("./DBService");
var FormTester = (function () {
    function FormTester(answers, encounterId, patientId, form_url, interval, frequency, isAsync, isDebug) {
        if (answers === void 0) { answers = [1, 100100000, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 7, 3, 0]; }
        if (encounterId === void 0) { encounterId = "7843903"; }
        if (patientId === void 0) { patientId = "333561"; }
        if (form_url === void 0) { form_url = "inject_dast"; }
        if (interval === void 0) { interval = 5; }
        if (frequency === void 0) { frequency = 2; }
        if (isAsync === void 0) { isAsync = false; }
        if (isDebug === void 0) { isDebug = false; }
        this.answers = answers;
        this.encounterId = encounterId;
        this.patientId = patientId;
        this.form_url = form_url;
        this.interval = process.argv[2] ? parseInt(process.argv[2]) : interval;
        this.frequency = process.argv[3] ? parseInt(process.argv[3]) : frequency;
        this.isAsync = true;
        this.host = this.isDebug ? 'localhost' : '10.18.6.41';
        this.counter = 0;
    }
    FormTester.prototype.submitForm = function (increment) {
        var _this = this;
        var payload = {
            'answers': this.answers.toString(),
            'encounterId': this.encounterId,
            'patientId': this.patientId
        };
        axios_1.default
            .post("http://" + this.host + "/Api/alerts/" + this.form_url, payload)
            .then(function (result) {
            console.log("ecw form submitted " + increment);
            _this.onEcwSubmitComplete(payload.answers, increment);
        })
            .catch(function (error) {
            var msg = error.response.data.Message ? error.response.data.Message.toString() : error.message;
            console.log("error writting form " + increment + " to ecw ");
            _this.onEcwSubmitFailure(payload.answers, msg, increment);
        });
    };
    FormTester.prototype.onEcwSubmitComplete = function (answers, increment) {
        var _this = this;
        var strSql = "insert into stress (timestamp,url, patientId, encounterId, data) \n              values ( '" + moment().format('YYYY-MM-DD hh:mm') + "','http://" + this.host + "/api/alerts/inject_dast'," + this.patientId + "," + this.encounterId + ",'" + answers.toString() + "')";
        DBService_1.DBService.mysqlQuery(strSql, function (results) {
            if (results.message == "")
                console.log("inserted stress test audit in DB");
            else
                console.log("error inserting stress test audit in DB" + results.message);
            _this.onDBSubmitComplete(increment);
        });
    };
    FormTester.prototype.onEcwSubmitFailure = function (answers, err, increment) {
        var _this = this;
        var strSql = "insert into stress (timestamp, url, errorDesc, patientId, encounterId, data ) \n              values ( '" + moment().format('YYYY-MM-DD hh:mm') + "', 'http://" + this.host + "/api/alerts/inject_dast',\n                            '" + err + "', " + this.patientId + ", " + this.encounterId + ", '" + answers.toString() + "')";
        DBService_1.DBService.mysqlQuery(strSql, function (results) {
            if (results.message == "")
                console.log("inserted stress test audit in DB");
            else
                console.log("error inserting stress test audit in DB" + results.message);
            _this.onDBSubmitComplete(increment);
        });
    };
    FormTester.prototype.onDBSubmitComplete = function (incrementId) {
        if (this.isAsync == false) {
            this.counter++;
            if (this.counter < this.frequency) {
                var submitId = this.counter;
                this.submitForm(submitId);
            }
            else {
                console.log("stress test serial completed after " + this.frequency + " requests ");
                process.exit();
            }
        }
        else {
            if (incrementId >= this.frequency) {
                console.log("stress test async completed after " + this.frequency + " requests ");
                clearInterval(this.timerReference);
                process.exit();
            }
        }
    };
    FormTester.prototype.runStressTestAsync = function () {
        var _this = this;
        console.log("begining stress test async interval every " + this.interval + " seconds, repeated " + this.frequency + " times");
        this.timerReference = setInterval(function () {
            _this.counter++;
            if (_this.counter <= _this.frequency) {
                var submitId = _this.counter;
                _this.submitForm(submitId);
            }
            else {
                // clearInterval(this.timerReference);
                //process.exit();
            }
        }, this.interval * 1000);
    };
    // you may be wondering why with promises and arrow functions 
    // those techniques are not being used for this test which relies on callbacks
    // the reason is a bug in node.js - this particular version + typescript which give a heap out of memory 
    // error with repeated nested callbacks - probably can refactor in another version
    FormTester.prototype.runStressTestSerial = function () {
        console.log("begining stress test serial run");
        this.counter++;
        var submitId = this.counter;
        this.submitForm(submitId);
    };
    return FormTester;
}());
exports.FormTester = FormTester;
var Tester1 = new FormTester();
//Tester1.runStressTestSerial();
Tester1.runStressTestAsync();
//# sourceMappingURL=stress-test.js.map